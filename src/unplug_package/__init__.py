#!/usr/bin/env python3

import importlib
import importlib.util
import sys
from pathlib import Path
import logging
from os.path import exists

class Plugins(object):

    def __init__(self):

        # define some lists
        self.plugins = []
        self.all_methods = []
        self.callable_methods = {}
        self.found_plugins_obj = []

        pathlist = Path('plugins/').glob('**/plugin.py')
        plugin_paths = []
        for path in pathlist:
            plugin_paths.append(path)
            path_split = str(path).split('/')
            plugin_name = path_split[1]
            self.plugins.append(plugin_name)
            spec = importlib.util.spec_from_file_location(plugin_name, path)
            foo = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(foo)

            # get available method names in all of our plugins
            found_methods = {foo.Plugin.__module__:[m for m in dir(foo.Plugin) if not m.startswith('__')]}
            
            # append found_methods to callable_methods dict
            self.callable_methods = {**self.callable_methods, **found_methods}

            # store our plugin object in a list so we can access it later
            self.found_plugins_obj.append(foo)

    def reload_plugin(self, plugin):
        print("\n** reloading a plugin")
        for p in self.found_plugins_obj:
            if plugin == p.__name__:
                # match found, we can try to reload
                print("match found, we can try to reload")
                pathlist = Path('plugins/').glob('**/plugin.py')
                for path in pathlist:
                    (junk, pname, junk2) = str(path).split('/')
                    if plugin == pname:
                        # matched the source file
                        spec = importlib.util.spec_from_file_location(plugin, path)
                        foo = importlib.util.module_from_spec(spec)
                        spec.loader.exec_module(foo)
                        foo.Plugin()
                        # now lets replace 
                        plug_index = 0
                        for old_plug in self.found_plugins_obj:
                            if old_plug.__name__ == plugin:
                                #print(f"found the index: {plug_index}")
                                self.found_plugins_obj[plug_index] = foo
                            else:
                                pass
                            plug_index += 1
            else:
                pass 
